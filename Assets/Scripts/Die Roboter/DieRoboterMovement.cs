﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DieRoboterMovement : MonoBehaviour {

	public float maximumSpeed;

	private Animator roboterAnimator;
	private bool isFacingRight = true;

	public LayerMask layerFloor;
	public LayerMask layerLava;
	public Transform groundCheckPositionA;
	public Transform groundCheckPositionB;
	private bool grounded = false;
	private bool takingDamage = false;
	private SpriteRenderer spriteRenderer;

	public float initialJumpForce;
	public float jumpForce;
	public float maxJumpHeight;
	public int hp = 3;
	public int maxHp = 6;
	public GameObject weaponContainer;
	public GameObject weapon1;
	public GameObject weapon2;
	public GameObject weapon3;
	public Color damageColor;
	public Color drawColor;
	private WeaponAttack weapon1Swing;
	private WeaponAttack weapon2Swing;
	private WeaponAttack weapon3Swing;
	public float invincibilityTimeAfterHit = 1f;
	public Image hpImage;
	private int lifeImagesDrawn = 0;
	public AudioSource playerHit;
	public AudioSource playerDraw;
	public AudioSource playerJump;

	private bool jumping = false;
	private bool stoppedJumping = false;
	private float jumpHeightDone;
	private bool attacking = false;
	private float verticalSpeed;
	private float playerHitBackValue = 0f;
	private float timerDamage = 0f;
	private float rightValue = -1f;//isfacingright para dano

	void Awake(){
		roboterAnimator = gameObject.GetComponent<Animator>();
		weapon1Swing = weapon1.GetComponent<WeaponAttack>();
		weapon2Swing = weapon2.GetComponent<WeaponAttack>();
		weapon3Swing = weapon3.GetComponent<WeaponAttack>();
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update (){
		//esta aqui para nao perder inputs no fixedupdate que pode pular frames
		//testei no fixedUpdate e de fato o input fica bem ruim
		if(!takingDamage){
			DieRoboterJump();
		}
		DieRoboterAttackButton();
		updateHPText();
		BellaLugosiIsDead();
	}

	void FixedUpdate(){
		if(takingDamage){
			HitBackLerp();
		}
		DieRoboterFalling();
		if(!takingDamage){
			DieRoboterRun();
		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == Tags.lava){
			hp = 0;
		}
	}

	void DieRoboterFalling(){
		grounded = (Physics2D.OverlapArea(groundCheckPositionA.position, groundCheckPositionB.position, layerFloor) && rigidbody2D.velocity.y == 0);
		roboterAnimator.SetBool("grounded", grounded);

		verticalSpeed = rigidbody2D.velocity.y;
		roboterAnimator.SetFloat("verticalSpeed", verticalSpeed);
	}

	void DieRoboterJump(){
		bool jumpingStart = Input.GetButtonDown("Jump");
		jumping = Input.GetButton("Jump");

		if(!jumping){
			stoppedJumping = true;
		}

		if(grounded && jumpingStart){
			jumpHeightDone = 0;
			stoppedJumping = false;
		}


		if(grounded && jumping && jumpingStart && jumpHeightDone == 0){
			//Debug.Log("pulo inicial");
			//som MUITO ruim... trocar
			//playerJump.Play();
			rigidbody2D.AddForce(new Vector2 (0, initialJumpForce));
			//transform.Translate(Vector2.up * initialJumpForce);
			jumpHeightDone += initialJumpForce;
		}

		if(/*!grounded &&*/ jumping && !jumpingStart && !stoppedJumping && jumpHeightDone < maxJumpHeight){
			//Debug.Log("pulo NAO inicial");
			rigidbody2D.AddForce(new Vector2 (0, jumpForce));
			//transform.Translate(Vector2.up * jumpForce);
			jumpHeightDone += jumpForce;
		}

		roboterAnimator.SetBool("jump", jumping);

		verticalSpeed = rigidbody2D.velocity.y;
		roboterAnimator.SetFloat("verticalSpeed", verticalSpeed);
	}

	void DieRoboterAttackButton(){
		if(Input.GetButtonDown("Fire1") /*&& attacking == false*/){
			DieRoboterAttack(weapon1, weapon1Swing);
		}
		if(Input.GetButtonDown("Fire2") /*&& attacking == false*/){
			DieRoboterAttack(weapon2, weapon2Swing);
		}
		if(Input.GetButtonDown("Fire3") /*&& attacking == false*/){
			DieRoboterAttack(weapon3, weapon3Swing);
		}
	}

	void DieRoboterAttack(GameObject weapon,WeaponAttack weaponSwing){
		attacking = true;
		weapon1.SetActive(false);
		weapon2.SetActive(false);
		weapon3.SetActive(false);
		weapon.SetActive(true);
		weaponSwing.SetStartingSwing(true);
	}

	void DieRoboterRun(){
		float horizontalAxisValue = Input.GetAxisRaw("Horizontal");
		float finalSpeedMovement;

		finalSpeedMovement = horizontalAxisValue * maximumSpeed;

		//Vector2 forceMovement = new Vector2(finalSpeedMovement, 0);

		if (horizontalAxisValue > 0 && !isFacingRight){
			Flip();
		}else{
			if (horizontalAxisValue < 0 && isFacingRight){
				Flip();
			}
		}

		//rigidbody2D.velocity = new Vector2 (finalSpeedMovement, rigidbody2D.velocity.y);

		//rigidbody2D.AddForce(forceMovement);

		transform.Translate(Vector2.right * finalSpeedMovement);

		//nao funcionou dessa forma personagem gruda no ponto, teleporta pra distancia da velocidade e volta pro ponto que ele grudou
		//transform.position = Vector2.right * finalSpeedMovement;

		roboterAnimator.SetFloat("speed", Mathf.Abs(finalSpeedMovement));
	}

	void Flip(){
			isFacingRight = !isFacingRight;

			Vector3 theScale = transform.localScale;

			theScale.x *= -1;
			transform.localScale = theScale;

			FlipWeapon();

			/*
			Vector3 flipVector = new Vector3(transform.rotation.x,-180,transform.rotation.z);
			transform.rotation = Quaternion.Euler(flipVector);
			*/
	}

	void FlipWeapon(){
		weaponContainer.transform.localScale = transform.localScale;
		float rotationYWeaponContainer = 0f;
		float rotationYWeapons = 0f;
		if(!isFacingRight){
			rotationYWeaponContainer = 180f;
		}
			Vector3 vectorWeaponContainer = new Vector3(weaponContainer.transform.rotation.x,rotationYWeaponContainer,weaponContainer.transform.rotation.z);
			weaponContainer.transform.rotation = Quaternion.Euler(vectorWeaponContainer);
			
			Vector3 vectorWeapon1 = new Vector3(weapon1.transform.rotation.x,rotationYWeapons,weapon1.transform.rotation.z);
			weapon1.transform.localRotation = Quaternion.Euler(vectorWeapon1);
			
			Vector3 vectorWeapon2 = new Vector3(weapon2.transform.rotation.x,rotationYWeapons,weapon2.transform.rotation.z);
			weapon2.transform.localRotation = Quaternion.Euler(vectorWeapon2);
			
			Vector3 vectorWeapon3 = new Vector3(weapon3.transform.rotation.x,rotationYWeapons,weapon3.transform.rotation.z);
			weapon3.transform.localRotation = Quaternion.Euler(vectorWeapon3);
	}

	public void DieRoboterTakeDamage(int damage, float playerHitBackValue){
		this.playerHitBackValue = playerHitBackValue;
		playerHit.Play();
		spriteRenderer.color = damageColor;
		HitBack();
		hp -= damage;
	}

	public void DieRoboterTakeDamageNoWeapon(int damage, float playerHitBackValue, float enemyXPosition){
		this.playerHitBackValue = playerHitBackValue;
		playerHit.Play();
		spriteRenderer.color = damageColor;
		HitBackNoWeapon(enemyXPosition);
		hp -= damage;
	}

	public void DieRoboterDrawKickBack(float playerHitBackValue){
		this.playerHitBackValue = playerHitBackValue;
		playerDraw.Play();
		spriteRenderer.color = drawColor;
		HitBack();
	}

	public void HitBack(){
		timerDamage = 0f;
		takingDamage = true;
		rightValue = -1;
		if(isFacingRight){
			rightValue = -1;
		}
		else{
			rightValue = 1;
		}
		//HitBackLerp(rightValue);
		//hitBackTotal = transform.position.x + (playerHitBackValue * rightValue);
		//takingDamage = false;
	}

	public void HitBackNoWeapon(float enemyXPosition){
		timerDamage = 0f;
		takingDamage = true;
		rightValue = -1;
		if(transform.position.x < enemyXPosition){
			rightValue = -1;
		}
		else{
			rightValue = 1;
		}
		//HitBackLerp(rightValue);
		//hitBackTotal = transform.position.x + (playerHitBackValue * rightValue);
		//takingDamage = false;
	}

	void HitBackLerp(){

		if (timerDamage < invincibilityTimeAfterHit){

			transform.Translate(new Vector2( (Vector2.right.x * playerHitBackValue * rightValue) , 0 ) );

			timerDamage += Time.deltaTime;	

			//transform.position = Vector2.Lerp(new Vector2(transform.position.x, transform.position.y), new Vector2(hitBackTotal, transform.position.y), invincibilityTimeAfterHit);
		}
		/*
		if( Mathf.Abs(transform.position.x - hitBackTotal) < 0.065 ){
		*/
		else{
			takingDamage = false;
			spriteRenderer.color = Color.white;
		}
		/*
		}
		*/
	}

	void updateHPText(){
		if(lifeImagesDrawn != hp && hp!= 0){
			hpImage.rectTransform.sizeDelta = new Vector2(hpImage.sprite.rect.width * hp, hpImage.rectTransform.sizeDelta.y);
			lifeImagesDrawn = hp;
		}
	}

	public bool getAttacking(){
		return attacking;
	}

	public void setAttacking(bool isAttacking){
		attacking = isAttacking;
	}

	public bool getTakingDamage(){
		return takingDamage;
	}

	public void setTakingDamage(bool isTakingDamage){
		takingDamage = isTakingDamage;
	}

	public bool getIsFacingRight(){
		return isFacingRight;
	}

	void BellaLugosiIsDead(){
		//Undead... Undead...
		if(hp <= 0){
			//Destroy(gameObject);
			Application.LoadLevel(Application.loadedLevelName);
		}
	}
}
