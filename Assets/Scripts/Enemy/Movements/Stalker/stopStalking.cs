﻿using UnityEngine;
using System.Collections;

public class stopStalking : MonoBehaviour {

	public GameObject enemy;
	private StalkerMovement stalkerMovement;
	
	void Awake () {
		stalkerMovement = enemy.GetComponent<StalkerMovement>();
	}
	
	void OnTriggerExit2D(Collider2D collision){
		if(collision.gameObject.tag == Tags.player){
			stalkerMovement.setIsStalking(false);
		}
	}
}
