﻿using UnityEngine;
using System.Collections;

public class startStalking : MonoBehaviour {
	public GameObject enemy;
	private StalkerMovement stalkerMovement;

	void Awake () {
		stalkerMovement = enemy.GetComponent<StalkerMovement>();
	}

	void OnTriggerEnter2D(Collider2D collision){
		if(collision.gameObject.tag == Tags.player){
			stalkerMovement.setIsStalking(true);
		}
	}
}
