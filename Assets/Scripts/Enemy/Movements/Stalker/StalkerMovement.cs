﻿using UnityEngine;
using System.Collections;

public class StalkerMovement : MonoBehaviour {

	public float velocity = 0.15f;
	public float timeIdleAfterHitPlayer = 1f;
	private GameObject stalkedPlayerObject;
	private bool isFacingRight = true;
	private bool isStalking = false;
	private bool playerHit = false;
	private float timeCountAfterHitPlayer = 0f;
	EnemyCollisions enemyCollisions;
	
	// Use this for initialization
	void Awake () {
		stalkedPlayerObject = GameObject.FindGameObjectWithTag(Tags.player);
		enemyCollisions = gameObject.GetComponent<EnemyCollisions>();
	}
	
	
	void FixedUpdate () {
		if(!enemyCollisions.getTakingDamage()){
			if(isStalking){

				Movement(stalkedPlayerObject.gameObject.transform.localPosition, velocity);
			}
		}
	}
	
	
	void Movement(Vector2 end, float velocity){
		if(transform.localPosition.x < end.x && !isFacingRight){
			Flip();
		}else{
			if(transform.localPosition.x > end.x && isFacingRight){
				Flip();
			}
		}

		if(!playerHit){
			timeCountAfterHitPlayer = 0f;
			transform.localPosition = Vector2.MoveTowards(transform.localPosition, end, velocity);
		}else{
			timeCountAfterHitPlayer += Time.deltaTime;
			if(timeCountAfterHitPlayer > timeIdleAfterHitPlayer){
				playerHit = false;
			}
		}
	}
	
	void Flip(){
		isFacingRight = !isFacingRight;
		
		Vector3 theScale = transform.localScale;
		
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == Tags.player){
			playerHit = true;
		}
	}

	public void setIsStalking(bool isStalking){
		this.isStalking = isStalking;
	}
}
