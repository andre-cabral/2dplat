﻿using UnityEngine;
using System.Collections;

public class ShowDogeSecretText : MonoBehaviour {

	private GameObject dogeSecretTexts;

	void Awake(){
		dogeSecretTexts = GameObject.FindGameObjectWithTag(Tags.secretDogeCanvas);
		dogeSecretTexts.SetActive(false);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == Tags.player){
			dogeSecretTexts.SetActive(true);
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if(other.gameObject.tag == Tags.player){
			dogeSecretTexts.SetActive(false);
		}
	}
}
