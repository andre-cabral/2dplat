﻿using UnityEngine;
using System.Collections;


public class RelativePointsMovement : MonoBehaviour {

	public Vector2[] points;
	public float velocity = 0.15f;
	private Vector2 startPosition;
	private int vectorIndex = 0;
	private bool isFacingRight = true;
	EnemyCollisions enemyCollisions;

	// Use this for initialization
	void Awake () {
		startPosition = transform.localPosition;
		enemyCollisions = gameObject.GetComponent<EnemyCollisions>();

		for(int i=0; i<points.Length; i++ ){
			if(i == 0){
				points[i] = new Vector2(points[i].x + startPosition.x, points[i].y + startPosition.y);
			}else{
				points[i] = new Vector2(points[i].x + points[i-1].x, points[i].y + points[i-1].y);
			}
		}
	}
	

	void FixedUpdate () {
		if(!enemyCollisions.getTakingDamage()){
			if(vectorIndex < points.Length){
				Movement(points[vectorIndex], velocity);
			}else{
				Movement(startPosition, velocity);
			}
		}
	}
	

	void Movement(Vector2 end, float velocity){
		if(transform.localPosition.x < end.x && !isFacingRight){
			Flip();
		}else{
			if(transform.localPosition.x > end.x && isFacingRight){
				Flip();
			}
		}

		transform.localPosition = Vector2.MoveTowards(transform.localPosition, end, velocity);
		if(transform.localPosition.x == end.x && transform.localPosition.y == end.y){
			if(vectorIndex < points.Length){
				vectorIndex++;
			}else{
				vectorIndex = 0;
			}
		}
	}

	void Flip(){
		isFacingRight = !isFacingRight;
		
		Vector3 theScale = transform.localScale;
		
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
