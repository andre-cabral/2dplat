﻿using UnityEngine;
using System.Collections;

public class ControllerSounds : MonoBehaviour {

	public AudioSource enemyHit;
	public AudioSource doorOpen;

	public void playEnemyHit(){
		enemyHit.Play();
	}

	public void playDoorOpen(){
		doorOpen.Play();
	}
}
