﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {

	public const string enemy = "Enemy";
	public const string gameController = "GameController";
	public const string player = "Player";
	public const string weapon = "Weapon";
	public const string secretDogeCanvas = "SecretDogeCanvas";
	public const string lava = "Lava";
	public const string hpImage = "HPImage";

	/*MainMenu*/
	public const string mainMenuNewGame = "MainMenuNewGame";
	public const string mainMenuExit = "MainMenuExit";

	/*PauseMenu*/
	public const string PauseMenuResume = "PauseMenuResume";
	public const string PauseMenuMainMenu = "PauseMenuMainMenu";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
