﻿using UnityEngine;
using System.Collections;

public class FinishLevel : MonoBehaviour {

	public GUIText finishedLevelText;
	public string nextLevel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == Tags.player){
			finishLevel();
		}
	}

	void finishLevel(){
		finishedLevelText.gameObject.SetActive(true);
		Application.LoadLevel(nextLevel);
	}
}
