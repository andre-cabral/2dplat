﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

	public Transform[] backgrounds;
	public float smooth;
	private float[] parallaxValues;
	private Transform cameraPosition;
	private Vector3 cameraPrevPosition;

	void Awake(){
		cameraPosition = Camera.main.transform;
	}

	void Start () {
		parallaxValues = new float[backgrounds.Length];

		for(int i=0; i<backgrounds.Length; i++){
			parallaxValues[i] = backgrounds[i].position.z * -1;
		}

		cameraPrevPosition = cameraPosition.position;
	}
	
	void Update () {
		for(int i=0; i<parallaxValues.Length; i++){
			float parallaxX = (cameraPrevPosition.x - cameraPosition.position.x) * parallaxValues[i];
			//float parallaxY = (cameraPrevPosition.y - cameraPosition.position.y) * parallaxValues[i];

			float backgroundTargetPosX = backgrounds[i].position.x + parallaxX;
			//float backgroundTargetPosY = backgrounds[i].position.y + parallaxY;

			//Vector3 targetPosition = new Vector3(backgroundTargetPosX, backgroundTargetPosY, backgrounds[i].position.z);
			Vector3 targetPosition = new Vector3(backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);
			backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, targetPosition, smooth * Time.deltaTime);
		}


		cameraPrevPosition = cameraPosition.position;
	}
}
