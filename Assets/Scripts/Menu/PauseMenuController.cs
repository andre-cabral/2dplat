﻿using UnityEngine;
using System.Collections;

public class PauseMenuController : MonoBehaviour {

	public GameObject[] menuOptions;
	public GameObject menuSelector;
	public GameObject menuBackground;
	public GameObject dieRoboter;
	private DieRoboterMovement dieRoboterMovement;
	public GameObject gameController;
	private AudioSource[] controllerSounds;
	private ArrayList playingSounds = new ArrayList();
	private int selectedOption = 0;
	private bool axisInUse = false;
	private bool paused = false;
	
	// Use this for initialization
	void Awake () {
		dieRoboterMovement = dieRoboter.GetComponent<DieRoboterMovement>();
		controllerSounds = gameController.GetComponents<AudioSource>();

		ResumeGame();
	}
	
	// Update is called once per frame
	void Update () {
		if(paused){
			ChangeSelector();
			SelectOption();
		}else{
			if(Input.GetButtonDown("Pause")){
				PauseGame();
			}
		}
	}
	
	
	void ChangeSelector(){
		if(Input.GetAxisRaw("Vertical") > 0 && !axisInUse){
			axisInUse = true;
			if(selectedOption > 0){
				selectedOption--;
			}else{
				selectedOption = menuOptions.Length-1;
			}
			UpdateSelector();
		}
		if(Input.GetAxisRaw("Vertical") < 0 && !axisInUse){
			axisInUse = true;
			if(selectedOption < menuOptions.Length-1){
				selectedOption++;
			}else{
				selectedOption = 0;
			}
			UpdateSelector();
		}
		if(Input.GetAxisRaw("Vertical") == 0){
			axisInUse = false;
		}
	}
	
	void UpdateSelector(){
		Vector3 selectorPosition = new Vector3(menuSelector.transform.position.x, menuOptions[selectedOption].transform.position.y, menuSelector.transform.position.z);
		menuSelector.transform.position = selectorPosition;
	}
	
	void SelectOption(){
		if( Input.GetButtonDown("Pause") || Input.GetKeyDown("escape") ){
			ResumeGame();
		}
		if(
		Input.GetKeyDown("enter") || Input.GetKeyDown("return") 
		|| Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire3") 
		|| Input.GetButtonDown("Jump") 
		){
			if(menuOptions[selectedOption].tag == Tags.PauseMenuResume){
				ResumeGame();
			}
			if(menuOptions[selectedOption].tag == Tags.PauseMenuMainMenu){
				Application.LoadLevel("MainMenu");
			}
		}
	}

	void PauseGame(){
		Time.timeScale = 0;
		paused = true;

		playingSounds.Clear();
		for (int i = 0; i < controllerSounds.Length; i++){
			if(controllerSounds[i].isPlaying){
				playingSounds.Add(i);
				controllerSounds[i].Pause();
			}
		}

		foreach (GameObject optionMenu in menuOptions){
			optionMenu.SetActive(true);
		}
		menuSelector.SetActive(true);
		menuBackground.SetActive(true);
		dieRoboterMovement.enabled = false;
	}

	void ResumeGame(){
		Time.timeScale = 1;
		paused = false;

		foreach (int audio in playingSounds){
			controllerSounds[audio].Play();
		}
		playingSounds.Clear();

		foreach (GameObject optionMenu in menuOptions){
			optionMenu.SetActive(false);
		}
		menuSelector.SetActive(false);
		menuBackground.SetActive(false);
		dieRoboterMovement.enabled = true;
	}
}
