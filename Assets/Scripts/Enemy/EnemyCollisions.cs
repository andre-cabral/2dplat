﻿using UnityEngine;
using System.Collections;

public class EnemyCollisions : MonoBehaviour {

	public string enemyType;
	public int enemyLifePoints = 2;
	public float enemyHitBackValue = 4f;
	public float enemyDrawHitBackValue = 2f;
	public Color enemyDamageColor;
	public Color enemyDrawColor;
	public float playerHitBackValue = 4f;
	public float playerDrawHitBackValue = 2f;
	public int playerDamageOnHit = 1;
	public float invincibilityTimeAfterHit = 1f;
	private bool takingDamage = false;
	private float hitBackTotal = 0f;
	private SpriteRenderer spriteRenderer;
	private GameObject gameController;
	private ControllerSounds controllerSounds;

	void Awake(){
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		gameController = GameObject.FindGameObjectWithTag(Tags.gameController);
		controllerSounds = gameController.GetComponent<ControllerSounds>();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		BellaLugosiIsDead();
	}

	void FixedUpdate(){
		if(takingDamage){
			HitBackLerp();
		}
	}


	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == Tags.player){
			DieRoboterMovement dieRoboterMovement = collision.gameObject.GetComponent<DieRoboterMovement>();

			if(!dieRoboterMovement.getTakingDamage() && !takingDamage){
				dieRoboterMovement.DieRoboterTakeDamageNoWeapon(playerDamageOnHit, playerHitBackValue, transform.position.x);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.gameObject.tag == Tags.weapon){

		}
	}

	public void WeaponWin(bool isFacingRight){
		TakeDamage(isFacingRight);
	}

	public void WeaponDraw(bool isFacingRight){
		DrawHitBack(isFacingRight);
		//rigidbody2D.AddForce();
	}

	public void WeaponLose(){
	}

	void TakeDamage(bool isFacingRight){
		HitBack(isFacingRight, enemyHitBackValue);
		spriteRenderer.color = enemyDamageColor;
		enemyLifePoints--;
	}

	void DrawHitBack(bool isFacingRight){
		HitBack(isFacingRight, enemyDrawHitBackValue);
		spriteRenderer.color = enemyDrawColor;
	}

	public void HitBack(bool isFacingRight, float hitBackValue){
		takingDamage = true;
		float rightValue = 1;
		if(isFacingRight){
			rightValue = 1;
		}
		else{
			rightValue = -1;
		}
		hitBackTotal = transform.position.x + (hitBackValue * rightValue);
	}

	void HitBackLerp(){
		transform.position = Vector2.Lerp(new Vector2(transform.position.x, transform.position.y), new Vector2(hitBackTotal, transform.position.y), invincibilityTimeAfterHit);
		if( Mathf.Abs(transform.position.x - hitBackTotal) < 0.065 ){
			takingDamage = false;
			spriteRenderer.color = Color.white;
		}
	}

	public bool getTakingDamage(){
		return takingDamage;
	}

	void BellaLugosiIsDead(){
		//Undead... Undead...
		if(enemyLifePoints <= 0){
			controllerSounds.playEnemyHit();
			Destroy(gameObject);
		}
	}
}
