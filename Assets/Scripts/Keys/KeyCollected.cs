﻿using UnityEngine;
using System.Collections;

public class KeyCollected : MonoBehaviour {

	public GameObject door;
	private GameObject gameController;
	private ControllerSounds controllerSounds;

	void Awake () {
		gameController = GameObject.FindGameObjectWithTag(Tags.gameController);
		controllerSounds = gameController.GetComponent<ControllerSounds>();
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == Tags.player){
			controllerSounds.playDoorOpen();
			Destroy(door);
			Destroy(gameObject);
		}
	}
}
