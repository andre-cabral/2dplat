﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

	public GameObject[] menuOptions;
	public GameObject menuSelector;
	private int selectedOption = 0;
	private bool axisInUse = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		ChangeSelector();
		SelectOption();
	}


	void ChangeSelector(){
		if(Input.GetAxisRaw("Vertical") > 0 && !axisInUse){
			axisInUse = true;
			if(selectedOption > 0){
				selectedOption--;
			}else{
				selectedOption = menuOptions.Length-1;
			}
			UpdateSelector();
		}
		if(Input.GetAxisRaw("Vertical") < 0 && !axisInUse){
			axisInUse = true;
			if(selectedOption < menuOptions.Length-1){
				selectedOption++;
			}else{
				selectedOption = 0;
			}
			UpdateSelector();
		}
		if(Input.GetAxisRaw("Vertical") == 0){
			axisInUse = false;
		}
	}

	void UpdateSelector(){
		Vector2 selectorPosition = new Vector2(menuSelector.transform.position.x, menuOptions[selectedOption].transform.position.y);
		menuSelector.transform.position = selectorPosition;
	}

	void SelectOption(){
		if(Input.GetKeyDown("enter") || Input.GetKeyDown("return") || Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire3") || Input.GetButtonDown("Jump") ){
			if(menuOptions[selectedOption].tag == Tags.mainMenuNewGame){
				Application.LoadLevel("HowToPlay");
			}
			if(menuOptions[selectedOption].tag == Tags.mainMenuExit){
				Application.Quit();
			}
		}
	}


}
