﻿using UnityEngine;
using System.Collections;

public class WeaponAttack : MonoBehaviour {

	public GameObject dieRoboter;
	private DieRoboterMovement dieRoboterMovement;

	private GameObject gameController;
	private RockPaperScissors rockPaperScissors;

	public string weaponType;
	public float weaponSpeed = 10f;
	public Vector3 weaponStartRotationVector = new Vector3(0f, 0f, 0f);
	public Vector3 weaponFinalRotationVector = new Vector3(0f, 0f, 180f);
	public float weaponFinalRotationFloat = -0.97f;

	private Quaternion weaponStartRotation;
	private Quaternion weaponFinalRotation;

	private bool startingSwing = false;
	private bool collisionHappened = false;

	private ArrayList alreadyCollided = new ArrayList();
	
	void Awake () {
		dieRoboterMovement = dieRoboter.GetComponent<DieRoboterMovement>();

		gameController = GameObject.FindGameObjectWithTag(Tags.gameController);
		rockPaperScissors = gameController.GetComponent<RockPaperScissors>();
	}

	void Start(){

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if(startingSwing){
			weaponStartRotation = Quaternion.Euler(weaponStartRotationVector);
			weaponFinalRotation = Quaternion.Euler(weaponFinalRotationVector);
			dieRoboterMovement.setAttacking(true);
			transform.localRotation = weaponStartRotation;

			startingSwing = false;
		}
		WeaponSwing();
	}


	void OnTriggerEnter2D(Collider2D other) {

		if(!alreadyCollided.Contains(other)){
			collisionHappened = false;
		}

		if(other.gameObject.tag == Tags.enemy && !collisionHappened){
			alreadyCollided.Add(other);
			collisionHappened = true;

			EnemyCollisions enemy = other.gameObject.GetComponent<EnemyCollisions>();
			string result = rockPaperScissors.WeaponResult(weaponType, enemy.enemyType);

			if(result == "Win"){
				enemy.WeaponWin(dieRoboterMovement.getIsFacingRight());
			}
			if(result == "Draw" && !dieRoboterMovement.getTakingDamage()){
				enemy.WeaponDraw(dieRoboterMovement.getIsFacingRight());
				dieRoboterMovement.DieRoboterDrawKickBack(enemy.playerDrawHitBackValue);
			}
			if(result == "Lose" && !dieRoboterMovement.getTakingDamage()){
				dieRoboterMovement.DieRoboterTakeDamage(enemy.playerDamageOnHit, enemy.playerHitBackValue);
				enemy.WeaponLose();
			}

		}
	}

	void WeaponSwing(){

		//uses the transform.rotation because the weaponStartRotation is not the position itself, and don't work with the Lerp
		//the transform.rotation receives the weaponStartRotation value instead to make the weapon start with the rotation specified 
		//on the inspector.
		transform.localRotation = Quaternion.Lerp(transform.localRotation, weaponFinalRotation, Time.deltaTime * weaponSpeed);

		if(transform.localRotation.z < weaponFinalRotationFloat ){
			dieRoboterMovement.setAttacking(false);
			gameObject.SetActive(false);
			collisionHappened = false;
			transform.localRotation = weaponStartRotation;
			alreadyCollided.Clear();
		}

	}

	public void SetStartingSwing(bool startingSwing){
		this.startingSwing = startingSwing;
	}
}
